#DESC: A Compute node in the Andromeda cluster

. andromeda-base.tmpl

# Enable the lustre client repo
YUM_CMD="${YUM_CMD} --enablerepo=lustre-client"

# Add some extra libraries and dev tools on the compute nodes
PKGLIST="${PKGLIST} \
    perl gtk2 atk cairo gcc gcc-c++ gcc-gfortran libxml2-python tcsh libnl tcl \
    numactl bc tk python cmake gdb valgrind distcc distcc-server \
    sysstat htop procps atlas-devel lapack-devel ncurses-devel \
    xorg-x11-xinit xorg-x11-server-Xorg mesa-libGL mesa-libOSMesa \
    mesa-libOSMesa-devel libXt-devel hdf5-devel netcdf-devel \
    python-devel bzip2-devel zlib-devel readline-devel expat-devel \
    libxml2-devel libarchive-devel xz-devel curl-devel freetype-devel \
    mpich mpich-devel mvapich mvapich-devel openmpi openmpi-devel
    libpng-devel libtiff-devel libjpeg-devel numpy scipy python-matplotlib"

rename_function configure_fstab andromeda_base_configure_fstab
configure_fstab() {
    if ! andromeda_base_configure_fstab "$@"; then
        return $?
    fi

    # Add the shared filesystem
    if ! grep lustre $CHROOTDIR/etc/fstab 2>&1 > /dev/null; then
        echo "storage-mds00-ib0@o2ib:/oort /data/shared lustre defaults,_netdev,noatime 0 0" >> $CHROOTDIR/etc/fstab
    fi

    return 0
}

rename_function configure_services andromeda_base_configure_services
configure_services() {
    if ! andromeda_base_configure_services "$@"; then
        return $?
    fi

    # Enable all memory to be used by InfiniBand devices
    if ! grep "log_num_mtt" $CHROOTDIR/etc/modprobe.d/mlx4.conf > /dev/null; then
        echo options mlx4_core log_num_mtt=20 log_mtts_per_seg=4 >> $CHROOTDIR/etc/modprobe.d/mlx4.conf
    fi

    # Enable the grid engine exec daemon
    if [ -f /opt/ge2011.11/default/common/sgeexecd ]; then
        cp /opt/ge2011.11/default/common/sgeexecd $CHROOTDIR/etc/init.d/sgeexecd.andromeda
        chroot $CHROOTDIR chkconfig --add sgeexecd.andromeda
        chroot $CHROOTDIR chkconfig sgeexecd.andromeda on
    fi

    # Enable the distcc compile server
    chroot $CHROOTDIR chkconfig distccd on
    sed 's|^#OPTIONS=.*|OPTIONS="--allow 172.16.1.0/24 --allow 172.16.2.0/24"|' -i $CHROOTDIR/etc/sysconfig/distccd


    # Configure the Lustre services to remount if necessary
    if ! grep "Mounting Lustre shares failed" $CHROOTDIR/etc/rc.local > /dev/null; then
        cat <<EOF >$CHROOTDIR/etc/rc.local

while ! mount | grep lustre; do
    echo "Mounting Lustre shares failed.  Retrying in 5s"
    sleep 5
    echo "Mounting Lustre shares..."
    mount -a -t lustre
done
EOF
    fi

    return 0
}

# Last minute configuration details
rename_function finalize andromeda_base_finalize
finalize() {
    if ! andromeda_base_finalize "$@"; then
        return $?
    fi

    # Setup up filesystem mount points
    mkdir -p $CHROOTDIR/data/shared

    return 0
}

# vim:filetype=sh:syntax=sh:expandtab:ts=4:sw=4:
